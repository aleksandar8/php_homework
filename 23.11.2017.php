

<?php 
// 1. Напиши скрипта во која ќе дефинираш некое име. Доколку името е еднкво на Kathrin скриптата ќе печати 'Hello Kathrin', а инаку ќе печати 'Nice name'.
echo "<br>";

$name = 'Kathrin';

if ($name == 'Kathrin'){
	echo 'Hello Kathrin';
}
else{
	echo 'Nice name';
} 
// 2. Прошири ја претходната скрипта на тој начин што со готовата функција од php, date() ќе земеш колку е часот. Доколку часот е пред 12 на пладне, испечати 'Good morning Kathrin', а во обратниот случај е 'Good afternoon or evening Kathrin'.
echo "<br>";

$name = 'Kathrin';
$time = date('H');

if ($time < 12){
	echo 'Good morning ' .''. $name;
}
else {
	echo 'Good afternoon ' .''. $name;
}

//3. Напишете скрипа во која ќе дефинирате променлива која е претставува рејтинг. Доколку рејтингот е помеѓу 1 и 10 тогаш тој е валиден и треба да испечатите порака 'Thank you for rating.', а доколку е невалиден испечатете 'Invalid rating.'.--->
echo "<br>";

$rating = 1;

if ($rating >= 1 && $rating <= 10){
	echo 'Thank you for rating.';
}
else{
	echo 'Invalid rating.';
}

// 4. Напишете скрипта во која што ќе дефинирате променлива за рејтинг и променлива која ќе претставува дали корисникот гласал до сега. (ex. $rated = true). За рејтингот важат претходните услови, но додаваме и нови дека доколку рејтингот е валиден, но корисникот гласал треба да се испечати порака 'You already voted.', а доколку корисникот не гласал да се исечати 'Thanks for voting'.
echo "<br>";
$rating = 1;
$rated = FALSE;

if ($rated == TRUE) {
 echo 'You already voted.';
}
else if ($rating >= 1 && $rating <= 10){
	echo 'Thank you for voting.';
}

// 5. Напиши скрипта во која ќе дефинираш некоја реченица. Доколку реченицата има повеќе од 20 карактери и повеќе од 4 зборови, конвертирај ги сите букви во големи и испечати ја, а доколку не го задоволува условот конвертирај ги сите букви во мали и испечати ја. Hint: користи готови функции од php: http://php.net/manual/en/ref.strings.php. -->

echo "<br>";
$text = 'Ne Znam KOLKU zborovi I BuKvi ImA';
		

if ( str_word_count($text) >= 4  && strlen( $text) >= 20 ) {
	echo strtoupper($text);
}
else {
	echo  strtolower($text);
}

		// echo "<br>";
		// echo str_word_count($text);
		// echo "<br>";
		// echo strlen( $text);
		// echo "<br>"; 
		// var_dump($text);



// 6. Напиши скрипта во која ќе дефинираш еден број од 1 до 12. Користејќи switch испечати на кој месец од годината соодвествува дефинираниот број. -->
echo "<br>";
$mesec = 7;
switch ($mesec) {
	case '1':
		echo 'Januari';
		break;
	case '2':
		echo 'Februari';
		break;
	case '3':
		echo 'Mart';
		break;
	case '4':
		echo 'April';
		break;
	case '5':
		echo 'Maj';
		break;
	case '6':
		echo 'Juni';
		break;
	case '7':
		echo 'Juli';
		
		break;
	case '8':
		echo 'Avgust';
		
		break;
	case '9':
		echo 'Septemvri';
		
		break;
	case '10':
		echo 'Oktomvri';
		
		break;
	case '11':
		echo 'Noemvri';
		
		break;
	case '12':
		echo 'Dekemvri';
		
		break;
		
}

 ?>
